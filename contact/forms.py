from django import forms
from .models import Message

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('name', 'email', 'message')


# W zasadzie to samo co powyzej tylko zrobione bez wykorzystania modelu
# plus przyklad walidacji
class ContactForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea())

    def clean_name(self):
        data = self.cleaned_data['name']
        if "D" not in data:
            raise forms.ValidationError("Musisz miec imie zawirajace litere 'D'!")
        return data
