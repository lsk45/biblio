from django.db import models
# from django.contrib.auth.models import User -  bardzo podstawowe
from django.conf import settings

from shelf.models import BookItem

from django.utils.timezone import now


class Rental(models.Model):
    who = models.ForeignKey(settings.AUTH_USER_MODEL) # domyślnie 'django.contrib.auth.User'
    what = models.ForeignKey(BookItem)
    # Jedno z dwóch rozwiązań automatycznej daty
    # when = models.DateTimeField(auto_now_add=True)
    when = models.DateTimeField(default=now)
    returned = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return "{who} - {what}, from {when} - to {ret}".format(who=self.who, what=self.what, when=self.when, ret=self.returned)
