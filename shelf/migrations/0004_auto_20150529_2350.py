# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shelf', '0003_auto_20150517_1015'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bookcategory',
            options={'verbose_name_plural': 'book categories', 'verbose_name': 'book category'},
        ),
        migrations.AlterField(
            model_name='bookedition',
            name='book',
            field=models.ForeignKey(to='shelf.Book', related_name='editions'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookedition',
            name='isbn',
            field=models.CharField(blank=True, max_length=17),
            preserve_default=True,
        ),
    ]
