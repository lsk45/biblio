Biblio
======

Biblio is a simple Django project to manage library books.

dependencies:

* defusedxml 0.4.1
* Django 1.7.7
* django-allauth 0.20.0
* django-configurations 0.8
* oauthlib 0.7.2
* Pillow 2.8.1
* python3-openid 3.0.5
* requests 2.7.0
* requests-oauthlib 0.5.0

## installing:

```
pip install -r requirements.txt
```

## how to run:
```
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```
